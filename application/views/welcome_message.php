
<body>
  <!-- banner section end -->
  <div class="banner_section layout_padding">
     <div id="main_slider" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
           <div class="carousel-item active">
              <div class="container">
                 <div class="row">
                    <div class="col-md-6">
                       <h1 class="banner_taital"style="color: #531D9E;">FED <span style="color: #E16310;">EX</span></h1>
                    </div>
                    <div class="col-md-6">
                       <div><img src="plantilla/images/banner1.jpeg" class="image_1"></div>
                    </div>
                 </div>
              </div>
           </div>
           <div class="carousel-item">
              <div class="container">
                 <div class="row">
                    <div class="col-md-6">
                       <h1 class="banner_taital"style="color: #531D9E;">FED <span style="color: #E16310;">EX</span></h1>
                    </div>
                    <div class="col-md-6">
                       <div><img src="plantilla/images/banner2.jpeg" class="image_1"></div>
                    </div>
                 </div>
              </div>
           </div>
           <div class="carousel-item">
              <div class="container">
                 <div class="row">
                    <div class="col-md-6">
                       <h1 class="banner_taital"style="color: #531D9E;">FED <span style="color: #E16310;">EX</span></h1>
                    </div>
                    <div class="col-md-6">
                       <div><img src="plantilla/images/banner3.jpeg" class="image_1"></div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
        <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="next">
           <i class="fa fa-angle-left"></i>
        </a>
        <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
           <i class="fa fa-angle-right"></i>
        </a>
     </div>
  </div>
  <div class="container">
     <div class="play_icon"><a href="#"><img src="plantilla/images/play-icon.png"></a></div>
  </div>
  <!-- banner section end -->
  <!-- about section start -->
   <div class="about_section layout_padding">
      <div id="my_main_slider" class="carousel slide" data-ride="carousel">
         <div class="carousel-inner">
            <div class="carousel-item active">
               <div class="container">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="about_main">
                           <h1><b>Nosotros</b></h1>
                           <p class="about_text">Creemos que una sociedad diversa es una sociedad más fuerte. Nos esforzamos por asegurar que nuestra compañía
                             refleje las muchas culturas que hay en nuestra fuerza de trabajo, nuestros clientes y nuestras comunidades alrededor del mundo.
                           </p>
                           <div class="readmore_bt"><a href="#">Leer mas</a></div>
                        </div>
                     </div>
                     <div class="col-md-6 ">
                        <div class="image_2"><img src="<?php echo base_url(); ?>/plantilla/images/fed.jpeg"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <a class="carousel-control-prev" href="#my_main_slider" role="button" data-slide="next">
            <i class="fa fa-angle-left"></i>
         </a>
         <a class="carousel-control-next" href="#my_main_slider" role="button" data-slide="next">
            <i class="fa fa-angle-right"></i>
         </a>
      </div>
   </div>
   <!-- about section end -->
   <!-- booking section start -->
   <div class="booking_section">
      <div class="container">
         <div class="row">
            <div class="col-md-6">
               <h1 class="booking_taital"></h1>
               <p class="booking_text"></p>
            </div>

         </div>
      </div>
   </div>
   <!-- booking section end -->
   <!-- services section start -->
   <div class="services_section layout_padding">
      <div class="container">
         <h1 class="text-center"><b>Galeria</b></h1>
         <p class="text-center">Lo que somos y lo que hacemos </p>
         <div class="services_section_2 layout_padding">
            <div class="row">
               <div class="col-md-6">
                  <div class="box_main active">
                     <div class="left_main">
                        <div class="cup_img_1"><img src="<?php echo base_url(); ?>/plantilla/images/fed1.jpeg"></div>
                     </div>
                     <div class="middle_main">
                        <div class="border_10 active"></div>
                     </div>
                     <div class="right_main">
                        <h2 class="milk_text">Carga</h2>
                        <h6 class="price_text">EEUU</h6>
                     </div>
                  </div>
                  <div class="box_main">
                     <div class="left_main">
                        <div class="cup_img_1"><img src="<?php echo base_url(); ?>/plantilla/images/fed2.jpeg"></div>
                     </div>
                     <div class="middle_main">
                        <div class="border_10"></div>
                     </div>
                     <div class="right_main">
                        <h6 class="milk_text">Entrega</h6>
                        <h1 class="price_text">Ruta</h1>
                     </div>
                  </div>
                  <div class="box_main">
                     <div class="left_main">
                        <div class="cup_img_1"><img src="<?php echo base_url(); ?>/plantilla/images/fed7.jpeg"></div>
                     </div>
                     <div class="middle_main">
                        <div class="border_10"></div>
                     </div>
                     <div class="right_main">
                        <h6 class="milk_text">Paqueteria</h6>
                        <h1 class="price_text">Recibo</h1>
                     </div>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="box_main">
                     <div class="left_main">
                        <div class="cup_img_1"><img src="<?php echo base_url(); ?>/plantilla/images/fed4.jpeg"></div>
                     </div>
                     <div class="middle_main">
                        <div class="border_10"></div>
                     </div>
                     <div class="right_main">
                        <h6 class="milk_text">Envíos</h6>
                        <h1 class="price_text">Latam</h1>
                     </div>
                  </div>
                  <div class="box_main">
                     <div class="left_main">
                        <div class="cup_img_1"><img src="<?php echo base_url(); ?>/plantilla/images/fed5.jpeg"></div>
                     </div>
                     <div class="middle_main">
                        <div class="border_10"></div>
                     </div>
                     <div class="right_main">
                        <h6 class="milk_text">Despacho</h6>
                        <h1 class="price_text">agencia</h1>
                     </div>
                  </div>
                  <div class="box_main">
                     <div class="left_main">
                        <div class="cup_img_1"><img src="<?php echo base_url(); ?>/plantilla/images/fed6.jpeg"></div>
                     </div>
                     <div class="middle_main">
                        <div class="border_10"></div>
                     </div>
                     <div class="right_main">
                        <h6 class="milk_text">Grupo De Trabajo</h6>
                        <h1 class="price_text">Mundial</h1>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="see_bt"><a href="#">Ver Más</a></div>
      </div>
   </div>
   <!-- services section end -->
   <!-- choose section start -->
   <div class="choose_section">
      <div class="container">
         <h1 class="choose_taital">Promociones y Descuentos</h1>
         <p class="choose_text">Tenemos los mejores precios y descuentos en el mercado laboral, oportunidades para nuestros clientes</p>
         <div class="choose_section_2">
            <div class="choose_left">
               <div class="choose_left_main">
                  <div class="icon_1"><img src="<?php echo base_url(); ?>/plantilla/images/feliz.png"></div>
               </div>
               <div class="choose_right_main">
                  <h1 class="satisfied_text">100%<br><span class="satisfied_text_1">Satisfactorio</span></h1>
               </div>
            </div>
            <div class="choose_middle">
               <div class="choose_left_main">
                  <div class="icon_1"><img src="<?php echo base_url(); ?>/plantilla/images/bolso.png"></div>
               </div>
               <div class="choose_right_main">
                  <h1 class="satisfied_text">220+<br><span class="satisfied_text_1">Destinos Mundiales</span></h1>
               </div>
            </div>
            <div class="choose_middle_1">
               <div class="choose_left_main">
                  <div class="icon_1"><img src="<?php echo base_url(); ?>/plantilla/images/grupo.png"></div>
               </div>
               <div class="choose_right_main">
                  <h1 class="satisfied_text">10000+<br><span class="satisfied_text_1">Trbajadores</span></h1>
               </div>
            </div>
            <div class="choose_right">
               <div class="choose_left_main">
                  <div class="icon_1"><img src="<?php echo base_url(); ?>/plantilla/images/barra.png"></div>
               </div>
               <div class="choose_right_main">
                  <h1 class="satisfied_text">100000+<br><span class="satisfied_text_1">Vistas Diarias</span></h1>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- choose section end -->
   <!-- shop section start -->
      <div class="shop_section layout_padding">
         <div id="my_slider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
               <div class="carousel-item active">
                  <div class="container">
                     <h1 class="text-center"><b>Multi-Destinos</b></h1>
                     <br>
                     <p class="text-center">Llegamos a varias partes del mundo, mas de 220 paises</p>
                     <div><img src="<?php echo base_url(); ?>/plantilla/images/ima1.jpeg" class="image_3"></div>
                  </div>
               </div>
               <div class="carousel-item">
                  <div class="container">
                     <h1 class="text-center"><b>Multi-Paqueteria</b></h1>
                     <br>
                     <p class="text-center">Cumplimos con un régimen ordenado con nuestros sistema de paqueteria, totalmente segura y eficiente. </p>
                     <div><img src="<?php echo base_url(); ?>/plantilla/images/ima2.jpeg" class="image_3"></div>
                  </div>
               </div>
               <div class="carousel-item">
                  <div class="container">
                     <h1 class="text-center"><b>Entrega Puerta A Puerta</b></h1>
                     <br>
                     <p class="text-center">Todos nuestros pedidos lo hacemos con entrega puerta a puerta para la seguridad de nuestros clientes.</p>
                     <div><img src="<?php echo base_url(); ?>/plantilla/images/ima3.jpeg" class="image_3"></div>
                  </div>
               </div>
            </div>
            <a class="carousel-control-prev" href="#my_slider" role="button" data-slide="next">
               <i class="fa fa-arrow-left"></i>
            </a>
            <a class="carousel-control-next" href="#my_slider" role="button" data-slide="next">
               <i class="fa fa-arrow-right" style="text-align: left;"></i>
            </a>
         </div>
      </div>
      <!-- shop section end -->
   <!-- frequently section start -->
   <div class="frequently_section layout_padding">
      <div class="container">
         <h1 class="text-center"><b>Preguntas frecuentes</b></h1>
         <p class="frequently_text">Dudas e inquietudes que tienen nuestros clientes, aqui encontraras las respuestas a tus preguntas </p>
         <div class="frequently_section_2 layout_padding">
            <div class="bs-example">
               <div class="accordion" id="accordionExample">
                  <div class="card">
                     <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link w-100" data-toggle="collapse"
                              data-target="#collapseOne"><i class="fa fa-plus"></i>¿FedEx solicita Datos personales?</button>
                        </h2>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed w-100" data-toggle="collapse"
                              data-target="#collapseTwo"><i class="fa fa-plus"></i>¿Qué necesito llevar para recoger un paquete en FedEx?</button>
                        </h2>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed w-100" data-toggle="collapse"
                              data-target="#collapseThree"><i class="fa fa-plus"></i>¿Cuándo puedo recoger los paquetes de FedEx?</button>
                        </h2>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed w-100" data-toggle="collapse"
                              data-target="#collapseThree"><i class="fa fa-plus"></i>¿Dónde puedo recoger mi entrega de FedEx?</button>
                        </h2>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                           <button type="button" class="btn btn-link collapsed w-100" data-toggle="collapse"
                              data-target="#collapseThree"><i class="fa fa-plus"></i>¿Por cuánto tiempo se retendrá mi paquete para retiro en una oficina?</button>
                        </h2>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- frequently section end -->

   <!-- contact section start -->
   <div class="contact_section layout_padding">
      <div class="container-fluid">
         <div class="row">
            <div class="col-md-6">
               <div class="mail_main">
                  <h1><b>Contáctanos</b></h1>
                  <br>
                  <form action="/action_page.php">
                     <div class="form-group">
                        <input type="text" class="email-bt" placeholder="Nombre" name="Nombre">
                     </div>
                     <div class="form-group">
                        <input type="text" class="email-bt" placeholder="Email" name="Email">
                     </div>
                     <div class="form-group">
                        <input type="text" class="email-bt" placeholder="Tema" name="Email">
                     </div>
                     <div class="form-group">
                        <textarea class="massage-bt" placeholder="Mensaje" rows="5" id="comment" name="text"></textarea>
                     </div>
                  </form>
                  <div class="send_btn">
                     <div class="main_bt"><a href="#">Enviar</a></div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="map_main">
                  <div class="map-responsive">
                     <iframe
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=6406 N Interstate Hwy 35 Ste 1210, Austin, TX 78752, Estados Unidos"
                        width="600" height="600" frameborder="0" style="border:0; width: 100%;"
                        allowfullscreen=""></iframe>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- contact section end -->
