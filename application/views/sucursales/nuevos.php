<br>
<main>
  <div class="row">
    <div class="col-md-12 text-center">
      <h1>~ NUEVA SUCURSAL ~</h1>
    </div>
  </div>
  <br>
    <form class=""
    action="<?php echo site_url(); ?>/sucursales/guardar"
    method="post">
        <div class="row">
          <div class="col-md-4">
            <label for="">Pais:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el pais"
            class="form-control"
            name="pais_suc" value=""
            id="pais_suc">
          </div>
          <div class="col-md-4">
            <label for="">Ciudad:</label>
            <br>
            <input type="text"
            placeholder="Ingrese la ciudad"
            class="form-control"
            name="ciudad_suc" value=""
            id="ciudad_suc">
          </div>
          <div class="col-md-4">
            <label for="">Nombre:</label>
            <br>
            <input type="text"
            placeholder="Ingrese el pais"
            class="form-control"
            name="nombre_suc" value=""
            id="nombre_suc">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4">
            <label for="">Teléfono:</label>
            <br>
            <input type="number"
            placeholder="Ingrese un número de contacto"
            class="form-control"
            name="telf_suc" value=""
            id="telf_suc">
          </div>
          <div class="col-md-4">
            <label for="">Latitud:</label>
            <br>
            <input type="number"
            placeholder="Ingrese la ubicación en el mapa"
            class="form-control" readonly
            name="latitud_suc" value=""
            id="latitud_suc">
          </div>
          <div class="col-md-4">
            <label for="">Longitud:</label>
            <br>
            <input type="number"
            placeholder="Ingrese la ubicación en el mapa"
            class="form-control" readonly
            name="longitud_suc" value=""
            id="longitud_suc">
          </div>
        </div>
          <br>

          <div class="row">
            <div class="col-md-12">
              <div id="mapaUbicacion" style="height:500px; width:100%; border:2px solid black;"></div>
            </div>
          </div>

          <script type="text/javascript">
            function initMap(){
              var centro=new google.maps.LatLng(-0.4116834386292155, -78.5489524702866);

              var mapa1=new google.maps.Map(
                document.getElementById('mapaUbicacion'),
                {
                  center:centro,
                  zoom:7,
                  mapTypeId:google.maps.MapTypeId.HYBRID
                }
              );
              var marcador=new google.maps.Marker({
                position:centro,
                map:mapa1,
                title:"Seleccione la direccion",
                icon:"<?php echo base_url();?>/assets/images/iconYellow.png",
                draggable:true //con este podemos mover el marcador el icono
              });
              google.maps.event.addListener(marcador, 'dragend', function(){
                // alert("Se termino el Drag");
              document.getElementById('latitud_suc').value=this.getPosition().lat();
              document.getElementById('longitud_suc').value=this.getPosition().lng();
            }); //le colocammos marcador porque queremos obtener los datos de la poscion que se encuentre el icono
            }//cierre dde la funcion initMap
          </script>
        <br><br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button"
                class="btn btn-primary">
                  Guardar
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/sucursales/indexs"
                  class="btn btn-danger">
                  Cancelar
                </a>
            </div>
        </div>
        <br>
    </form>
</main>
