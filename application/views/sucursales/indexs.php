<main>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-md-6">
          <h1>~ LISTADO DE SUCURSALES ~</h1>
      </div>
      <div class="col-md-6">
        <br>
        <a href="<?php echo site_url('sucursales/nuevos');?>"class="btn btn-primary">
          <i class="glyphicon glyphicon-plus"></i>Agregar Sucursal</a>
      </div>
    </div>
    <br>
    <?php if ($sucursales): ?>
      <table class="table table-striped table-bordered">
        <thead class="bg-primary">
          <tr>
            <th>ID</th>
            <th>PAIS</th>
            <th>CIUDAD</th>
            <th>NOMBRE</th>
            <th>TELEFONO</th>
            <th>LATITUD</th>
            <th>LONGITUD</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($sucursales as $filaTemporal): ?>
            <tr>
              <td><?php echo $filaTemporal->id_suc?></td>
              <td><?php echo $filaTemporal->pais_suc?></td>
              <td><?php echo $filaTemporal->ciudad_suc?></td>
              <td><?php echo $filaTemporal->nombre_suc?></td>
              <td><?php echo $filaTemporal->telf_suc?></td>
              <td><?php echo $filaTemporal->latitud_suc?></td>
              <td><?php echo $filaTemporal->longitud_suc?></td>
              <td class="text-center">
                <a href="#" title="Editar Sucursal">
                  <i class="glyphicon glyphicon-pencil"></i>
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url("sucursales/eliminar/$filaTemporal->id_suc")?>"title="Eliminar Sucursal" onclick="return confirm ('¿Desea eliminar este registro?');" style="color:red">
                 <i class="glyphicon glyphicon-trash"></i>
               </a>
             </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h1>No hay datos</h1>
    <?php endif; ?>
  </div>
</main>
