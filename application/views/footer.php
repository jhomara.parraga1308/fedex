<!-- footer section start -->
  <div class="footer_section layout_padding">
     <div class="container">
        <div class="newsletter_section">
           <div class="newsletter_left">
              <div class="footer_logo"><img src="<?php echo base_url(); ?>/plantilla/images/logo.png"></div>
           </div>
        </div>
        <div class="footer_taital_main">
           <div class="row">
              <div class="col-lg-3 col-sm-6">
                 <h2 class="useful_text">NUESTRA COMPAÑÍA</h2>
                 <div class="footer_links">
                    <ul>
                       <li><a href="index.html">Acerca de FedEx</a></li>
                       <li><a href="about.html">Nuestro Portafolio</a></li>
                       <li><a href="services.html">Relaciones con inversionistas</a></li>
                       <li><a href="pricing.html">Empleos</a></li>
                    </ul>
                 </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                 <h2 class="useful_text">Menus</h2>
                 <div class="footer_links">
                    <ul>
                       <li><a href="index.html">Responsabilidad empresarial</a></li>
                       <li><a href="about.html">Sala de redacción</a></li>
                       <li><a href="services.html">Comuníquese con nosotros</a></li>
                    </ul>
                 </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                 <h2 class="useful_text">MÁS SOBRE FEDEX</h2>
                 <div class="footer_links">
                    <ul>
                       <li><a href="#">Compatibilidad con FedEx</a></li>
                       <li><a href="#">FedEx Developer Portal</a></li>
                       <li><a href="#">FedEx Logistics</a></li>
                       <li><a href="#">FedEx Cross Border</a></li>
                    </ul>
                 </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                 <h2 class="useful_text">Contact us</h2>
                 <div class="addres_link">
                    <ul>
                       <li><a href="#"><img src="<?php echo base_url(); ?>/plantilla/images/map-icon.png"><span class="padding_left_10">Entregas Especiales, S.A.</span></a></li>
                       <li><a href="#"><img src="<?php echo base_url(); ?>/plantilla/images/call-icon.png"><span class="padding_left_10">1800 033 339</span></a></li>
                       <li><a href="#"><img src="<?php echo base_url(); ?>/plantilla/images/mail-icon.png"><span
                                class="padding_left_10">info@fedexcourierserviceonline.ec</span></a></li>
                    </ul>
                 </div>
              </div>
           </div>
           <h1 class="follow_text">Siguenos en </h1>
           <div class="social_icon">
              <ul>
                 <li><a href="#"><img src="<?php echo base_url(); ?>/plantilla//plantilla/images/fb-icon.png"></a></li>
                 <li><a href="#"><img src="<?php echo base_url(); ?>/plantilla/images/twitter-icon.png"></a></li>
                 <li><a href="#"><img src="<?php echo base_url(); ?>/plantilla/images/linkedin-icon.png"></a></li>
                 <li><a href="#"><img src="<?php echo base_url(); ?>/plantilla/images/instagram-icon.png"></a></li>
              </ul>
           </div>
        </div>
     </div>
  </div>
  <!-- footer section end -->
  <!-- copyright section start -->
  <div class="copyright_section">
     <div class="container">
        <p class="copyright_text">Copyright 2019 All Right Reserved By.<a href="https://html.design"> Free html
              Templates</a> Distributed by. <a href="https://themewagon.com">ThemeWagon</a> </p>
        </p>
     </div>
  </div>
  <!-- copyright section end -->
  <!-- Javascript files-->
  <script src="<?php echo base_url(); ?>/plantilla/js/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/js/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/js/jquery-3.0.0.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/js/plugin.js"></script>
  <!-- sidebar -->
  <script src="<?php echo base_url(); ?>/plantilla/js/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/js/custom.js"></script>
  <!-- javascript -->
  <script src="<?php echo base_url(); ?>/plantilla/js/owl.carousel.js"></script>
  <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
