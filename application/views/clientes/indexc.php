<main>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-md-6">
          <h1>~ LISTADO DE CLIENTES ~</h1>
      </div>
      <div class="col-md-6">
        <br>
        <a href="<?php echo site_url('clientes/nuevoc');?>"class="btn btn-primary">
          <i class="glyphicon glyphicon-plus"></i>Agregar Cliente</a>
      </div>
    </div>
    <br>
    <?php if ($clientes): ?>
      <table class="table table-striped table-bordered">
        <thead class="bg-primary">
          <tr>
            <th>ID</th>
            <th>CEDULA</th>
            <th>NOMBRE</th>
            <th>APELLIDO</th>
            <th>CIUDAD</th>
            <th>PAIS</th>
            <th>TELEFONO</th>
            <th>LATITUD</th>
            <th>LONGITUD</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($clientes as $filaTemporal): ?>
            <tr>
              <td><?php echo $filaTemporal->id_cli?></td>
              <td><?php echo $filaTemporal->cedula_cli?></td>
              <td><?php echo $filaTemporal->nombre_cli?></td>
              <td><?php echo $filaTemporal->apellido_cli?></td>
              <td><?php echo $filaTemporal->ciudad_cli?></td>
              <td><?php echo $filaTemporal->pais_cli?></td>
              <td><?php echo $filaTemporal->telf_cli?></td>
              <td><?php echo $filaTemporal->latitud_cli?></td>
              <td><?php echo $filaTemporal->longitud_cli?></td>
              <td class="text-center">
                <a href="#" title="Editar Cliente">
                  <i class="glyphicon glyphicon-pencil"></i>
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url("clientes/eliminar/$filaTemporal->id_cli")?>"title="Eliminar Cliente" onclick="return confirm ('¿Desea eliminar este registro?');" style="color:red">
                 <i class="glyphicon glyphicon-trash"></i>
               </a>
             </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h1>No hay datos</h1>
    <?php endif; ?>
  </div>
</main>
