<br>
<main>
  <div class="row">
    <div class="col-md-12 text-center">
      <h1>~ NUEVO CLIENTE ~</h1>
    </div>
  </div>
  <br>
    <form class=""
    action="<?php echo site_url(); ?>/clientes/guardar"
    method="post">
        <div class="row">
          <div class="col-md-4">
            <label for="">Cedula: <span class="obligatorio">(obligatorio)</span></label>
            <br>
            <input type="number"
            placeholder="Ingrese la cedula"
            class="form-control" required
            name="cedula_cli" value=""
            id="cedula_cli">
          </div>
          <div class="col-md-4">
            <label for="">Nombre:<span class="obligatorio">(obligatorio)</span></label>
            <br>
            <input type="text"
            placeholder="Ingrese su nombre"
            class="form-control"  required
            name="nombre_cli" value=""
            id="nombre_cli">
          </div>
          <div class="col-md-4">
            <label for="">Apellido:<span class="obligatorio">(obligatorio)</span></label>
            <br>
            <input type="text"
            placeholder="Ingrese su apellido"
            class="form-control" required
            name="apellido_cli" value=""
            id="apellido_cli">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4">
            <label for="">Ciudad:<span class="obligatorio">(obligatorio)</span></label>
            <br>
            <input type="text"
            placeholder="Ingrese su ciudad"
            class="form-control" required
            name="ciudad_cli" value=""
            id="ciudad_cli">
          </div>
          <div class="col-md-4">
            <label for="">Pais:<span class="obligatorio">(obligatorio)</span></label>
            <br>
            <input type="text"
            placeholder="Ingrese su pais"
            class="form-control" required
            name="pais_cli" value=""
            id="pais_cli">
          </div>
          <div class="col-md-4">
            <label for="">Telefono:<span class="obligatorio">(obligatorio)</span></label>
            <br>
            <input type="number"
            placeholder="Ingrese un numero de contacto"
            class="form-control" required
            name="telf_cli" value=""
            id="telf_cli">
          </div>
        </div>
          <br>
        <div class="row">
          <div class="col-md-6">
            <label for="">Latitud:</label>
            <br>
            <input type="number"
            placeholder="Ingrese su ubicacion"
            class="form-control" readonly
            name="latitud_cli" value=""
            id="latitud_cli">
          </div>
          <div class="col-md-6">
            <label for="">Longitud:</label>
            <br>
            <input type="number"
            placeholder="Ingrese su ubicacion"
            class="form-control" readonly
            name="longitud_cli" value=""
            id="longitud_cli">
          </div>
        </div>
        <br>
        </div>
          <div class="row">
            <div class="col-md-12">
              <div id="mapaUbicacionC" style="height:500px; width:100%; border:2px solid black;"></div>
            </div>
          </div>

          <script type="text/javascript">
            function initMap(){
              var centro=new google.maps.LatLng(-0.4116834386292155, -78.5489524702866);

              var mapa2=new google.maps.Map(
                document.getElementById('mapaUbicacionC'),
                {
                  center:centro,
                  zoom:7,
                  mapTypeId:google.maps.MapTypeId.HYBRID
                }
              );
              var marcador2=new google.maps.Marker({
                position:centro,
                map:mapa2,
                title:"Seleccione la direccion",
                icon:"<?php echo base_url();?>/assets/images/iconYellow.png",
                draggable:true //con este podemos mover el marcador el icono
              });
              google.maps.event.addListener(marcador2, 'dragend', function(){
                // alert("Se termino el Drag");
              document.getElementById('latitud_cli').value=this.getPosition().lat();
              document.getElementById('longitud_cli').value=this.getPosition().lng();
            }); //le colocammos marcador porque queremos obtener los datos de la poscion que se encuentre el icono
            }//cierre dde la funcion initMap
          </script>
        <br><br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button"
                class="btn btn-primary">
                  Guardar
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/clientes/indexc"
                  class="btn btn-danger">
                  Cancelar
                </a>
            </div>
        </div>
        <br>
    </form>

    <!-- para validar -->
    <!-- <script type="text/javascript">
      $("#frm__cliente").validate({
        rules:{
            cedula_cli:{
              required:true,
              minlength:10,
              maxlength:10,
              //digits:true
            },

            nombre_cli:{
              required:true,
              minlength:2,
              maxlength:150,
              letras:true
            },

            apellido_cli:{
              required:true,
              minlength:2,
              maxlength:150,
              letras:true
            },

            ciudad_cli:{
              required:true,
              minlength:2,
              maxlength:150,
              letras:true
            },

            pais_cli:{
              required:true,
              minlength:2,
              maxlength:150,
              letras:true
            },

            telf_cli:{
              required:true,
              minlength:10,
              maxlength:10,
              digits:true
            },
        },
        messages:{
          cedula_cli:{
            required:"Ingrese el numero de cedula",
            minlength:"Cédula incorrecta, Ingrese 10 digitos",
            maxlength:"Cédula incorrecta, Ingrese 10 digitos",
            digits:"Este campo solo acepta números",
            number:"Este campo solo acepta números"
          },

          nombre_cli:{
            required:"Porfavor ingrese su nombre",
            minlength:"El nombre debe tener al menos 2 caracteres",
            maxlength:"Nombre incorrecto"
            letras:"Este campo solo acepta letras"
          },

          apellido_cli:{
            required:"Porfavor ingrese su apellido.",
            minlength:"El apellido debe tener al menos 2 caracteres",
            maxlength:"Apellido incorrecto"
          },

          ciudad_cli:{
            required:"Porfavor ingrese el la ciudad de procedencia",
            minlength:"La ciuadd debe tener al menos 2 caracteres",
            maxlength:"Ciudad incorrecta"
          },

          pais_cli:{
            required:"Porfavor ingrese su pais de procedencia",
            minlength:"El pais debe tener al menos 2 caracteres",
            maxlength:"Pais incorrecto"
          },
          telf_cli:{
            required:"Ingrese el número de teléfono",
            minlength:"Teléfono incorrecto, Ingrese 10 digitos",
            maxlength:"Teléfono incorrecto, Ingrese 10 digitos",
            digits:"Este campo solo acepta números",
            number:"Este campo solo acepta números"
          },

        }
      });
    </script> -->
</main>
