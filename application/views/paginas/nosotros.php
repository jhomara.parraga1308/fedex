<!-- about section start -->
 <div class="about_section layout_padding">
    <div id="my_main_slider" class="carousel slide" data-ride="carousel">
       <div class="carousel-inner">
          <div class="carousel-item active">
             <div class="container">
                <div class="row">
                   <div class="col-md-6">
                      <div class="about_main">
                         <h1><b>Nosotros</b></h1>
                         <p class="about_text">Creemos que una sociedad diversa es una sociedad más fuerte. Nos esforzamos por asegurar que nuestra compañía
                           refleje las muchas culturas que hay en nuestra fuerza de trabajo, nuestros clientes y nuestras comunidades alrededor del mundo. Crea tu futuro.
                         </p>
                         <div class="readmore_bt"><a href="#">Leer mas</a></div>
                      </div>
                   </div>
                   <div class="col-md-6 ">
                      <div class="image_2"><img src="<?php echo base_url(); ?>/plantilla/images/fed.jpeg"></div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <a class="carousel-control-prev" href="#my_main_slider" role="button" data-slide="next">
          <i class="fa fa-angle-left"></i>
       </a>
       <a class="carousel-control-next" href="#my_main_slider" role="button" data-slide="next">
          <i class="fa fa-angle-right"></i>
       </a>
    </div>
 </div>
 <!-- about section end -->
