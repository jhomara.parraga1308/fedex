<main>
	<h1 class="text-center">~ REPORTE RUTA ~</h1>
	<div class="row">
    <div class="col-md-10">
			<div id="mapaLugares" style="height:600px; width:100%; border:2px solid black;"></div>
		</div>
    <div class="col-md-2">
			<br>
			<table class="table table-striped table-hover table-responsive" >
					<tr>
							<th class="text-center"><img src="<?php echo base_url();?>/assets/images/iconPed.png"></th>
							<td class="text-center">~ INICIO PEDIDO ~</td>
					</tr>
					<tr>
							<th class="text-center"><img src="<?php echo base_url();?>/assets/images/iconPed1.png"></th>
							<td class="text-center">~ DESTINO PEDIDO ~</td>
					</tr>
			</table>
		</div>

	</div>

	<div class="row">
    <div class="col-md-6 text-center">
      <br>
      <a href="<?php echo site_url('pedidos/nuevop');?>"class="btn btn-success">
        <i class="glyphicon glyphicon-hand-up"></i> Nuevo Pedido</a>
    </div>
    <div class="col-md-6 text-center">
      <br>
      <a href="<?php echo site_url('pedidos/indexp');?>"class="btn btn-info">
        <i class="glyphicon glyphicon-list"></i> Listado de  Pedidos</a>
    </div>
  </div>
  <br>
	<div class="row">
		<div class="col-md-12">
			<div id="mapaLugaresP" style="height:600px; width:100%; border:2px solid black;"></div>
		</div>
	</div>

<script type="text/javascript">

function initMap()
{
	const directionsRenderer = new google.maps.DirectionsRenderer();
	const directionsService = new google.maps.DirectionsService();
	var centro= new google.maps.LatLng(-0.4116834386292155, -78.5489524702866);
	var mapaLugaresP= new google.maps.Map(document.getElementById('mapaLugaresP'),
		{
			center:centro,
			zoom:7,
			mapTypeId:google.maps.MapTypeId.HYBRID
		}
	);

	directionsRenderer.setMap(mapaLugaresP);
  calculateAndDisplayRoute(directionsService, directionsRenderer);

	<?php if($lugaresPed): ?>
		var coorInicio=new google.maps.LatLng(<?php echo $pedidoRuta->latitud_ini_ped;?>,<?php echo $pedidoRuta->longitud_ini_ped;?>);
		var marcador=new google.maps.Marker({
			position:coorInicio,
			title:"<?php echo $pedidoRuta->nombre_ped; echo ' -  ' ;echo $pedidoRuta->tiempo_ped;?>",
			icon:"<?php echo base_url();?>/assets/images/iconPed.png",
			map:mapaLugaresP
		});

		var coorSalida=new google.maps.LatLng(<?php echo $pedidoRuta->latitud_des_ped;?>,<?php echo $pedidoRuta->longitud_des_ped;?>);
		var marcador=new google.maps.Marker({
			position: coorSalida,

			title:"<?php echo $pedidoRuta->nombre_ped; echo ' -  ' ;echo $pedidoRuta->tiempo_ped;?>",
			icon:"<?php echo base_url();?>/assets/images/iconPed1.png",
			map:mapaLugaresP
		});


	<?php endif; ?>

}//Cierre de la funcion initMap que no se entiende como es que sale que initMap no es una funcion???

	function calculateAndDisplayRoute(directionsService, directionsRenderer) {


  directionsService
    .route({
      origin: { lat: <?php echo $lugarTemporal->latitud_ini_ped;?>, lng: <?php echo $lugarTemporal->longitud_ini_ped;?> },
      destination: { lat: <?php echo $lugarTemporal->latitud_des_ped;?>, lng: <?php echo $lugarTemporal->longitud_des_ped;?> },

      travelMode: google.maps.TravelMode.DRIVING,
    })
    .then((response) => {
      directionsRenderer.setDirections(response);
    })
    .catch((e) => window.alert("Directions request failed due to " + status));
}


</script>
</main>




</main>
