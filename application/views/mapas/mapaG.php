<main>
	<h1 class="text-center">~ REPORTE GENERAL ~</h1>
	<div class="row">
    <div class="col-md-10">
			<div id="mapaLugares" style="height:600px; width:100%; border:2px solid black;"></div>
		</div>
    <div class="col-md-2">
			<br>
			<table class="table table-striped table-hover table-responsive" >
					<tr>
							<th class="text-center"><img src="<?php echo base_url();?>/assets/images/iconSuc.png"></th>
							<td class="text-center">~ SUCURSALES ~</td>
					</tr>
					<tr>
							<th class="text-center"><img src="<?php echo base_url();?>/assets/images/iconUser.png"></th>
							<td class="text-center">~ CLIENTES ~</td>
					</tr>
					<tr>
							<th class="text-center"><img src="<?php echo base_url();?>/assets/images/iconPed.png"></th>
							<td class="text-center">~ PEDIDOS ~</td>
					</tr>
			</table>
		</div>

	</div>

<script type="text/javascript">

function initMap()
{
	var centro= new google.maps.LatLng(-0.4116834386292155, -78.5489524702866);
	//Permite construir el mapa
	var mapaLugaresGenerales= new google.maps.Map(
		document.getElementById('mapaLugares'),
		{
			center:centro,
			zoom:5,
			mapTypeId:google.maps.MapTypeId.HYBRID
		}
	);

	<?php if($lugaresSuc && $lugaresCli && $lugaresPed): ?>
		<?php foreach ($lugaresSuc as $lugarTemporal): ?>
		var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_suc;?>,<?php echo $lugarTemporal->longitud_suc;?>);
		var marcador=new google.maps.Marker({
			position:coordenadaTemporal,
			title:"<?php echo $lugarTemporal->nombre_suc; echo ' - ' ;echo $lugarTemporal->pais_suc;?>",
			icon:"<?php echo base_url();?>/assets/images/iconSuc.png",
			map:mapaLugaresGenerales
		});
		<?php endforeach; ?>

    <?php foreach ($lugaresCli as $lugarTemporal): ?>
		var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_cli;?>,<?php echo $lugarTemporal->longitud_cli;?>);
		var marcador=new google.maps.Marker({
			position:coordenadaTemporal,
			title:"<?php echo $lugarTemporal->nombre_cli; echo ' - ' ;echo $lugarTemporal->pais_cli;?>",
			icon:"<?php echo base_url();?>/assets/images/iconUser.png",
			map:mapaLugaresGenerales
		});
		<?php endforeach; ?>

		<?php foreach ($lugaresPed as $lugarTemporal): ?>
		var coorInicio=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_ini_ped;?>,<?php echo $lugarTemporal->longitud_ini_ped;?>);
		var marcador=new google.maps.Marker({
			position:coorInicio,
			title:"<?php echo $lugarTemporal->nombre_ped; echo ' - ' ;echo $lugarTemporal->tiempo_ped;?>",
			icon:"<?php echo base_url();?>/assets/images/iconPed.png",
			map:mapaLugaresGenerales
		});
		var coorSalida=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_des_ped;?>,<?php echo $lugarTemporal->longitud_des_ped;?>);
		var marcador=new google.maps.Marker({
			position:coorSalida,
			title:"<?php echo $lugarTemporal->nombre_ped; echo ' - ' ;echo $lugarTemporal->tiempo_ped;?>",
			icon:"<?php echo base_url();?>/assets/images/iconPed.png",
			map:mapaLugaresGenerales
		});

		<?php endforeach; ?>
	<?php endif; ?>
	}//Cierre de la funcion initMap
</script>
</main>
