<main>
	<h1 class="text-center">~ SUCURSALES ~</h1>
  <div class="row">
    <div class="col-md-6 text-center">
      <br>
      <a href="<?php echo site_url('sucursales/nuevos');?>"class="btn btn-success">
        <i class="glyphicon glyphicon-hand-up"></i> Nueva Sucursal</a>
    </div>
    <div class="col-md-6 text-center">
      <br>
      <a href="<?php echo site_url('sucursales/indexs');?>"class="btn btn-info">
        <i class="glyphicon glyphicon-list"></i> Listado de  Sucursales</a>
    </div>
  </div>
  <br>
	<div class="row">
		<div class="col-md-12">
			<div id="mapaLugaresS" style="height:600px; width:100%; border:2px solid black;"></div>
		</div>
	</div>

<script type="text/javascript">

function initMap()
{
	var centro= new google.maps.LatLng(-0.4116834386292155, -78.5489524702866);
	var mapaLugaresS= new google.maps.Map(
		document.getElementById('mapaLugaresS'),
		{
			center:centro,
			zoom:7,
			mapTypeId:google.maps.MapTypeId.HYBRID
		}
	);
	<?php if($lugaresSuc): ?>
		<?php foreach ($lugaresSuc as $lugarTemporal): ?>
		var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_suc;?>,<?php echo $lugarTemporal->longitud_suc;?>);
		var marcador=new google.maps.Marker({
			position:coordenadaTemporal,
			title:"<?php echo $lugarTemporal->pais_suc; echo ' -  ' ;echo $lugarTemporal->nombre_suc;?>",
			icon:"<?php echo base_url();?>/assets/images/iconSuc.png",
			map:mapaLugaresS
		});
		<?php endforeach; ?>
	<?php endif; ?>
}//Cierre de la funcion initMap que no se entiende como es que sale que initMap no es una funcion???
</script>
</main>
