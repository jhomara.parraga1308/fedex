<!-- header section start -->
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>FedEx</title>

   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>FEDEX</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- bootstrap css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/plantilla/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/plantilla/css/style.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/responsive.css">
      <!-- fevicon -->
      <link rel="icon" href="<?php echo base_url(); ?>/plantilla/images/fevicon.png" type="image/gif" />
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/jquery.mCustomScrollbar.min.css">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <!-- owl stylesheets -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>/plantilla/css/owl.carousel.min.css">
      <link rel="stylesoeet" href="<?php echo base_url(); ?>/plantilla/css/owl.theme.default.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
         media="screen">

         <!-- CDN DE JQuery -->
         <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
         <!-- CDN DE BOOTSTRAP -->
         <!-- Latest compiled and minified CSS -->
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
         <!-- Optional theme -->
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
         <!-- Latest compiled and minified JavaScript -->
         <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
         <!-- IMPORTACION DE API KEY DE GOOGLE MAPS -->
           <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY9ocfqoH51fXQd4wPpnTsc1D7oxrXU0A&libraries=places&callback=initMap">
         </script>
         <!-- Aqui vamos a poner un codigo para validar que solo ingrese letras y caracteress del espanol -->
             <script type="text/javascript">
                   jQuery.validator.addMethod("letras", function(value, element) {
                     //return this.optional(element) || /^[a-z]+$/i.test(value);
                     return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);

                   }, "Este campo solo acepta letras");
                 </script>
             <!-- importamos el archivo js de toastr , unna libreria para mensajes de alerta -->
             <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
                 <!-- Este es el eesttilo css de toastr -->
             <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
   <div class="header_section">
      <nav class="navbar navbar-expand-lg navbar-light ">
         <div class="logo"><img src= "<?php echo base_url(); ?>/plantilla/images/logo.png"></div>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
         </button>
         <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
               <a class="nav-item nav-link" href="<?php echo base_url(); ?>">Home</a>
               <a class="nav-item nav-link" href="<?php echo site_url('paginas/nosotros'); ?>">Nosotros</a>
               <a class="nav-item nav-link" href="<?php echo site_url('mapas/mapaS');?>">Sucursales</a>
               <a class="nav-item nav-link" href="<?php echo site_url('mapas/mapaC'); ?>">Clientes</a>
               <a class="nav-item nav-link" href="<?php echo site_url('mapas/mapaP'); ?>">Pedidos</a>
               <a class="nav-item nav-link" href="<?php echo site_url('mapas/mapaG'); ?>">Actividad</a>
               <a class="nav-item nav-link" href="<?php echo site_url('paginas/contactos'); ?>">Contactos</a>
            </div>
         </div>
         <div class="login_menu">
            <a href="#"><img src="<?php echo base_url(); ?>plantilla/images/search-icon.png"></a>
         </div>
      </nav>

   </div>
   <!-- header section end -->

<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>FedEx</title>
  </head>

  <body>


    <nav class="navbar navbar-light">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
       <span class="sr-only">Toggle navigation</span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
     </button>
     <a class="navbar-brand" href="<?php echo site_url(); ?>"></a>
    </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav navbar-nav">
             <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Servicios<span class="caret"></span></a>
               <ul class="dropdown-menu">
                 <li><a href="<?php echo site_url(); ?>/sucursales/nuevos"<>Nueva sucursal</a></li>
                 <li><a href="<?php echo site_url(); ?>/sucursales/indexs">Listado sucursal</a></li>
                 <li><a href="<?php echo site_url(); ?>/clientes/nuevoc">Nuevo Cliente</a></li>
                 <li><a href="<?php echo site_url(); ?>/clientes/indexc">Listado Clientes</a></li>
                 <li><a href="<?php echo site_url(); ?>/pedidos/nuevop">Nuevo pedido</a></li>
                 <li><a href="<?php echo site_url(); ?>/pedidos/indexp">Listado pedido</a></li>
               </ul>
             </li>
           </ul>
           <ul class="nav navbar-nav">
             <li class="dropdown">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reportes <span class="caret"></span></a>
               <ul class="dropdown-menu">
                 <li><a href="<?php echo site_url(); ?>/mapas/mapaS">Reporte Sucursales</a></li>
                 <li><a href="<?php echo site_url(); ?>/mapas/mapaP">Reporte Pedidos</a></li>
                 <li><a href="<?php echo site_url(); ?>/mapas/mapaC">Reporte Clientes</a></li>
                    <li><a href="<?php echo site_url(); ?>/mapas/mapaG">Reporte General</a></li>
               </ul>
             </li>
           </ul>


          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </head>
