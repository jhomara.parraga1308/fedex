<main>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-md-6">
          <h1>~ LISTADO DE PEDIDOS ~</h1>
      </div>
      <div class="col-md-6">
        <br>
        <a href="<?php echo site_url('pedidos/nuevop');?>"class="btn btn-primary">
          <i class="glyphicon glyphicon-plus"></i>Agregar Pedido</a>
      </div>
    </div>
    <br>
    <?php if ($pedidos): ?>
      <table class="table table-striped table-bordered">
        <thead class="bg-primary">
          <tr>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>PESO</th>
            <th>PRECIO</th>
            <th>TIEMPO</th>
            <th>LATITUD INICIO</th>
            <th>LONGITUD INICIO</th>
            <th>LATITUD DESTINO</th>
            <th>LONGITUD DESTINO</th>
            <th>ACCIONES</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($pedidos as $filaTemporal): ?>
            <tr>
              <td><?php echo $filaTemporal->id_ped?></td>
              <td><?php echo $filaTemporal->nombre_ped?></td>
              <td><?php echo $filaTemporal->peso_ped?></td>
              <td><?php echo $filaTemporal->precio_ped?></td>
              <td><?php echo $filaTemporal->tiempo_ped?></td>
              <td><?php echo $filaTemporal->latitud_ini_ped?></td>
              <td><?php echo $filaTemporal->longitud_ini_ped?></td>
              <td><?php echo $filaTemporal->latitud_des_ped?></td>
              <td><?php echo $filaTemporal->latitud_des_ped?></td>
              <td class="text-center">
                <a href="#" title="Editar Pedido" style="color:black">
                  <i class="glyphicon glyphicon-pencil"></i>
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/pedidos/rutaP/<?php echo $filaTemporal->id_ped ?>" title="Ver Pedido" style="color:blue">
                  <i class="glyphicon glyphicon-globe"></i>
                </a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url("pedidos/eliminar/$filaTemporal->id_ped")?>"title="Eliminar Pedido" onclick="return confirm ('¿Desea eliminar este registro?');" style="color:red">
                 <i class="glyphicon glyphicon-trash"></i>
               </a>
             </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    <?php else: ?>
      <h1>No hay datos</h1>
    <?php endif; ?>
  </div>
</main>
