<br>
<main>
  <div class="row">
    <div class="col-md-12 text-center">
      <h1>~ NUEVO PEDIDO ~</h1>
    </div>
  </div>
  <br>
    <form class=""
    action="<?php echo site_url(); ?>/pedidos/guardar"
    method="post">
        <div class="row">
          <div class="col-md-3">
            <label for="">Nombre:</label>
            <br>
            <input type="text" placeholder="Ingrese un nombre de pedido" class="form-control" name="nombre_ped" value="" id="nombre_ped">
          </div>
          <div class="col-md-3">
            <label for="">Peso:</label>
            <br>
            <input type="text" placeholder="Ingrese el peso" class="form-control" name="peso_ped" value="" id="peso_ped">
          </div>
          <div class="col-md-3">
            <label for="">Precio:</label>
            <br>
            <input type="number" placeholder="Ingrese el presio del pedido" class="form-control" name="precio_ped" value="" id="precio_ped">
          </div>
          <div class="col-md-3">
            <label for="">Tiempo:</label>
            <br>
            <input type="date" placeholder="Ingrese el tiempo de tardanza" class="form-control" name="tiempo_ped" value="" id="tiempo_ped">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-6">
            <label for="">Latitud origen:</label>
            <br>
            <input type="text" placeholder="Ingrese una direccion" class="form-control" readonly name="latitud_ini_ped" value="" id="latitud_ini_ped">
          </div>
          <div class="col-md-6">
            <label for="">Longitud origen:</label>
            <br>
            <input type="number"
            placeholder="Ingrese una direccion" class="form-control" readonly name="longitud_ini_ped" value="" id="longitud_ini_ped">
          </div>
        </div>
          <br>
        <div class="row">
          <div class="col-md-6">
            <label for="">Latitud destino:</label>
            <br>
            <input type="number" placeholder="Ingrese su ubicacion" class="form-control" readonly name="latitud_des_ped" value="" id="latitud_des_ped">
          </div>
          <div class="col-md-6">
            <label for="">Longitud destino:</label>
            <br>
            <input type="number" placeholder="Ingrese su ubicacion" class="form-control" readonly name="longitud_des_ped" value="" id="longitud_des_ped">
          </div>
        </div>

        <br>
          <div class="row">
            <div class="col-md-12">
              <div id="mapaUbicacion3" style="height:500px; width:100%; border:2px solid black;"></div>
            </div>
          </div>

          <script type="text/javascript">
            function initMap(){
              var centro=new google.maps.LatLng(-0.4116834386292155, -78.5489524702866);

              var mapa3=new google.maps.Map(
                document.getElementById('mapaUbicacion3'),
                {
                  center:centro,
                  zoom:7,
                  mapTypeId:google.maps.MapTypeId.HYBRID
                }
              );

              //Primer marcador

              var marcadorIniPed=new google.maps.Marker({
                position:centro,
                map:mapa3,
                title:"Seleccione la direccion de inicio",
                icon:"<?php echo base_url();?>/assets/images/iconYellow.png",
                draggable:true //con este podemos mover el marcador el icono
              });
              google.maps.event.addListener(marcadorIniPed, 'dragend', function(){
                // alert("Se termino el Drag");
              document.getElementById('latitud_ini_ped').value=this.getPosition().lat();
              document.getElementById('longitud_ini_ped').value=this.getPosition().lng();

            }); //le colocammos marcador porque queremos obtener los datos de la poscion que se encuentre el icono

            // Segundo marcador

            var marcadorDesPed=new google.maps.Marker({
              position:centro,
              map:mapa3,
              title:"Seleccione la direccion del destino",
              icon:"<?php echo base_url();?>/assets/images/iconBlue.png",
              draggable:true //con este podemos mover el marcador el icono
            });
            google.maps.event.addListener(marcadorDesPed, 'dragend', function(){
              // alert("Se termino el Drag");
            document.getElementById('latitud_des_ped').value=this.getPosition().lat();
            document.getElementById('longitud_des_ped').value=this.getPosition().lng();
          });

            }//cierre dde la funcion initMap
          </script>
        <br><br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button"
                class="btn btn-primary">
                  Guardar
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/pedidos/indexp"
                  class="btn btn-danger">
                  Cancelar
                </a>
            </div>
        </div>
        <br>
    </form>
</main>

<script type="text/javascript">
  $("#frm_nuevo_instructor").validate({
    rules:{
      cedula_ins:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true,
      },
      primer_apellido_ins:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      segundo_apellido_ins:{
        minlength:3,
        maxlength:250,
        letras:true,
      },
      nombres_ins:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true,
      },
      titulo_ins:{
        required:true,
        minlength:2,
        maxlength:250,
        letras:true,
      },
      telefono_ins:{
        required:true,
        minlength:3,
        maxlength:250,
        digits:true,
      },
      direccion_ins:{
        required:true,
        minlength:3,
        maxlength:250,

      }
    },
    messages:{
      cedula_ins:{
        required:"Por favor ingrese el numero de cedula",
        minlength:"Cedula incorrecta,ingrese 10 digitos",
        maxlength:"Cedula incorrecta,ingrese 10 digitos",
        digits:"Este campo solo acepta números",
        number:"Este campo solo acepta números",
      },
      primer_apellido_ins:{
        required:"Por favor ingrese el apellido",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"Apellido Incorrecto",
      },
      segundo_apellido_ins:{
        required:"Por favor ingrese su segundo apellido",
        minlength:"El apellido debe tener al menos 3 caracteres",
        maxlength:"Apellido Incorrecto",

      },
      nombres_ins:{
        required:"Por favor ingrese el nombre",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Nombre Incorrecto",
      },
      titulo_ins:{
        required:"Por favor ingrese el título",
        minlength:"El título debe tener al menos 2 caracteres",
        maxlength:"Título No Existe",
      },
      telefono_ins:{
        required:"Por favor ingrese el numero de teléfono",
        minlength:"Teléfono incorrecto,ingrese 10 digitos",
        maxlength:"Teléfono incorrecto,ingrese 10 digitos",
        digits:"Este campo solo acepta números",
        number:"Este campo solo acepta números",
      },
        direccion_ins:{
          required:"Por favor ingrese la dirección",
          minlength:"La dirección debe tener al menos 3 caracteres",
          maxlength:"Dirección Incorrecta",
        }
    }
  });
</script>
