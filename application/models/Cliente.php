<?php
class Cliente extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insertar($datos){
    //proteccion con el Active record
    return $this->db->insert("Cliente",$datos);
  }

  function obtenerTodos(){
    $listadoClientes=$this->db->get("Cliente");
    if($listadoClientes->num_rows()>0){
      return $listadoClientes->result();
    }else{
      return false;
    }
  }

  function borrar($id_cli){
       $this->db->where("id_cli",$id_cli);
         return $this->db->delete("Cliente");
       }

    //
    // //sirve para editar una persona, se debe editar con base a un id, usadno where
    //     public function obtenerPorId($id_cli){
    //       $this->db->where("id_cli", $id_cli);
    //       $cliente=$this->db->get("Cliente");
    //       if($cliente->num_rows()>0){
    //         return $cliente->row();
    //       }
    //     }
    //
    //     function actualizar($id_cli,$datos){
    //       $this->db->where("id_cli",$id_cli);
    //       return $this->db->update("Cliente",$datos);
        
  }//cierre de la clase cliente
