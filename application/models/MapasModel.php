<?php

class MapasModel extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function obtenerTodos(){
     $listadoCliente=$this->db->get('Cliente');
     if($listadoCliente->num_rows()>0){
       return $listadoCliente->result();
     }
     return false;
   }

  function obtenerTodosSucursales(){

    $listadoSuc=$this->db->get("Sucursal");
    if ($listadoSuc->num_rows()>0){
        return $listadoSuc->result();
      }
      return false;
  }

  function obtenerTodosClientes(){
    $listadoCli=$this->db->get("Cliente");
    if ($listadoCli->num_rows()>0){
      return $listadoCli->result();
    }
    return false;
  }
  function obtenerTodosPedidos(){
    $listadoPed=$this->db->get("Pedido");
    if ($listadoPed->num_rows()>0){
      return $listadoPed->result();
    }
    return false;
  }
} //fin de la class

 ?>
