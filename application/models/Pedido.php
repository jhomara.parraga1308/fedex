<?php
class Pedido extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insertar($datos){
    //proteccion con el Active record
    return $this->db->insert("Pedido",$datos);
  }

  function obtenerTodos(){
    $listadoPedidos=$this->db->get("Pedido");
    if($listadoPedidos->num_rows()>0){
      return $listadoPedidos->result();
    }else{
      return false;
    }
  }

  function borrar($id_ped){
       $this->db->where("id_ped",$id_ped);
       if ($this->db->delete("Pedido")) {
         return true;
       } else {
         return false;
       }

    }

    public function obtenerPedidoPorId($id_ped){
    $this->db->where("id_ped", $id_ped);
    $pedido=$this->db->get("Pedido");
    if($pedido->num_rows()>0){
      return $pedido->row();
    }
  }

  }//cierre de la clase
