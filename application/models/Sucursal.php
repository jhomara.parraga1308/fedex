<?php
class Sucursal extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  function insertar($datos){
    //proteccion con el Active record
    return $this->db->insert("Sucursal",$datos);
  }

  function obtenerTodos(){
    $listadoSucursales=$this->db->get("Sucursal");
    if($listadoSucursales->num_rows()>0){
      return $listadoSucursales->result();
    }else{
      return false;
    }
  }

  function borrar($id_suc){
       $this->db->where("id_suc",$id_suc);
       if ($this->db->delete("Sucursal")) {
         return true;
       } else {
         return false;
       }

    }
  }//cierre de la clase
