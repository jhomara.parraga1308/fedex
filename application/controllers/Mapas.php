<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mapas extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("MapasModel");
  }

  public function mapaS()
  {
  	$dataS["lugaresSuc"]= $this->MapasModel->obtenerTodosSucursales();
  	$this->load->view('header');
  	$this->load->view('mapas/mapaS', $dataS);
  	$this->load->view('footer');
  }
  public function mapaC()
  {
  	$dataC["lugaresCli"]= $this->MapasModel->obtenerTodosClientes();
  	$this->load->view('header');
  	$this->load->view('mapas/mapaC', $dataC);
    $this->load->view('footer');
  }
  public function mapaP()
  {
  	$dataP["lugaresPed"]= $this->MapasModel->obtenerTodosPedidos();
  	$this->load->view('header');
  	$this->load->view('mapas/mapaP', $dataP);
  	$this->load->view('footer');
  	}

  public function mapaG()
  {
    $data["lugaresSuc"]=$this->MapasModel->obtenerTodosSucursales();
    $data["lugaresCli"]=$this->MapasModel->obtenerTodosClientes();
    $data["lugaresPed"]=$this->MapasModel->obtenerTodosPedidos();
    $this->load->view('header');
  	$this->load->view('mapas/mapaG', $data);
  	$this->load->view('footer');
  }

}// cierre de class

 ?>
