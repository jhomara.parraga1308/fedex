<?php
/**
 *
 */
class Paginas extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
  }

  public function nosotros(){
    $this->load->view('header');
    $this->load->view('paginas/nosotros');
    $this->load->view('footer');
  }

  public function contactos(){
    $this->load->view('header');
    $this->load->view('paginas/contactos');
    $this->load->view('footer');
  }

} //cierre

 ?>
