<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller {

  function __construct()
  {
    parent:: __construct();
    $this->load->model('Pedido');
  }

  //funcion para listar las sucursales
	public function indexp()
	{
    $data['pedidos']=$this->Pedido->obtenerTodos();
		$this->load->view('header');
		$this->load->view('pedidos/indexp',$data);
		$this->load->view('footer');
	}

  public function nuevop()
    {
      $this->load->view('header');
      $this->load->view('pedidos/nuevop');
      $this->load->view('footer');
    }

  public function guardar(){
        $datosNuevoPedido=array(
          "nombre_ped"=>$this->input->post('nombre_ped'),
          "peso_ped"=>$this->input->post('peso_ped'),
          "precio_ped"=>$this->input->post('precio_ped'),
          "tiempo_ped"=>$this->input->post('tiempo_ped'),
          "latitud_ini_ped"=>$this->input->post('latitud_ini_ped'),
          "longitud_ini_ped"=>$this->input->post('longitud_ini_ped'),
          "latitud_des_ped"=>$this->input->post('latitud_des_ped'),
          "longitud_des_ped"=>$this->input->post('longitud_des_ped')
        );
        if ($this->Pedido->
        insertar($datosNuevoPedido)){
          redirect('pedidos/indexp');
        }else {
            echo "Error al insertar un pedido";
        }
          // code...
      }
      public function eliminar($id_ped){
              if ($this->Pedido->borrar($id_ped)) {
                  redirect('pedidos/indexp');
              } else {
                  echo "ERROR AL BORRAR :(";
              }
          }

      public function rutaP($id_ped){
        $data["pedidoRuta"]=$this->Pedido->obtenerPedidoPorId($id_ped);
        $this->load->view('header');
    		$this->load->view('mapas/mapaR',$data);
    		$this->load->view('footer');
      }
} //cierre de la clse
