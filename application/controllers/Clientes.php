<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

  function __construct()
  {
    parent:: __construct();
    $this->load->model('Cliente');
  }

  //funcion para listar las sucursales
	public function indexc()
	{
    $data['clientes']=$this->Cliente->obtenerTodos();
		$this->load->view('header');
		$this->load->view('clientes/indexc',$data);
		$this->load->view('footer');
	}

  public function nuevoc()
    {
      $this->load->view('header');
      $this->load->view('clientes/nuevoc');
      $this->load->view('footer');
    }

  public function guardar(){
        $datosNuevoCliente=array(
          "cedula_cli"=>$this->input->post('cedula_cli'),
          "nombre_cli"=>$this->input->post('nombre_cli'),
          "apellido_cli"=>$this->input->post('apellido_cli'),
          "ciudad_cli"=>$this->input->post('ciudad_cli'),
          "pais_cli"=>$this->input->post('pais_cli'),
          "telf_cli"=>$this->input->post('telf_cli'),
          "latitud_cli"=>$this->input->post('latitud_cli'),
          "longitud_cli"=>$this->input->post('longitud_cli')
        );
        if ($this->Cliente->
        insertar($datosNuevoCliente)){
          redirect('clientes/indexc');
        }else {
            echo "Error al insertar un cliente";
        }
          // code...
      }
      public function eliminar($id_cli){
              if ($this->Cliente->borrar($id_cli)) {
                  redirect('clientes/indexc');
              } else {
                  echo "ERROR AL BORRAR :(";
              }
          }
} //cierre de la clse
