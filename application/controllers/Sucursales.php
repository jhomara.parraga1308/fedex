<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursales extends CI_Controller {

  function __construct()
  {
    parent:: __construct();
    $this->load->model('Sucursal');
  }

  //funcion para listar las sucursales
	public function indexs()
	{
    $data['sucursales']=$this->Sucursal->obtenerTodos();
		$this->load->view('header');
		$this->load->view('sucursales/indexs',$data);
		$this->load->view('footer');
	}

  public function nuevos()
    {
      $this->load->view('header');
      $this->load->view('sucursales/nuevos');
      $this->load->view('footer');
    }

  public function guardar(){
        $datosNuevoSucursal=array(
          "pais_suc"=>$this->input->post('pais_suc'),
          "ciudad_suc"=>$this->input->post('ciudad_suc'),
          "nombre_suc"=>$this->input->post('nombre_suc'),
          "telf_suc"=>$this->input->post('telf_suc'),
          "latitud_suc"=>$this->input->post('latitud_suc'),
          "longitud_suc"=>$this->input->post('longitud_suc')
        );
        if ($this->Sucursal->
        insertar($datosNuevoSucursal)){
          redirect('sucursales/indexs');
        }else {
            echo "Error al insertar una sucursal";
        }
          // code...
      }
      public function eliminar($id_suc){
              if ($this->Sucursal->borrar($id_suc)) {
                  redirect('sucursales/indexs');
              } else {
                  echo "ERROR AL BORRAR :(";
              }
          }
} //cierre de la clse
